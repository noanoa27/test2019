import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
//לטעון את המחלקה אובסרוובל מתוך המחלקה rxjs
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  logOut() {
    return this.fireBaseAuth.auth.signOut();
  }
 
  signup(email:string,password:string){
    return this.fireBaseAuth
     .auth
     .createUserWithEmailAndPassword(email,password);
   }
  
   updateProfile(user, name:string) {
    user.updateProfile({displayName: name, photoURL: ''});
  }
 
  login(email: string, password: string) {
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email, password);
  }
  addUser(user,name:string,nickname:string){
    let uid = user.uid;
    let ref = this.db.database.ref('/'); //האנד פוינט הראשי של דטה בייס
    ref.child('users').child(uid).push({'name':name,'nickname':nickname}); //יוצרת אנד פוינט חדש
  }
  user: Observable<firebase.User>;

  constructor(private fireBaseAuth:AngularFireAuth,
    private db:AngularFireDatabase) { 
this.user=fireBaseAuth.authState;
}

}
