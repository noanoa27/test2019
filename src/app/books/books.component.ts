import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BooksService } from '../books.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  books=[];
  name:string;
  authName:string;

  constructor(private db:AngularFireDatabase,
   public authService:AuthService,
   private booksService:BooksService) { }

   
   addBook() {
    this.booksService.addBook(this.name,this.authName);
    this.name = '';
    this.authName= '';
  }


   ngOnInit() {
    this.authService.user.subscribe(
      user => {
        if(!user) return;
        this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe( // tell me for all the data changes on the things below
          books => { // the array he took from the FireBase
            this.books = [];
            books.forEach(
              book => {
                let y = book.payload.toJSON();
                y['$key'] = book.key;
                this.books.push(y);
              }
            )
          }
        )
        
      }
    )
  }
 

}
