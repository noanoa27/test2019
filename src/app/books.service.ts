import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
@Injectable({
  providedIn: 'root'
})
export class BooksService {

  addBook(book: string,authName:string) {
    this.authService.user.subscribe(
      user => {
        this.db.list('/users/'+user.uid+'/books').push({'name': book,'authName': authName});
      }
    )
  }
  update(key, text1,text2) {
    this.authService.user.subscribe(
      user => {
        this.db.list('users/'+user.uid+'/books').update(key,{'name':text1,'authName': text2});
      }
    )
  }
  updateDone(key:string, text:string, done:boolean)
 {
   this.authService.user.subscribe(user =>{
     this.db.list('/users/'+user.uid+'/books').update(key,{'name':text, 'read':done});
  })
 
 }

 
  constructor(private authService:AuthService,
    private db:AngularFireDatabase,
  ) { }

}
