import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import {FormControl, Validators} from '@angular/forms';

import {Router} from "@angular/router";
@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  name: string;
  password: string;
  nickname: string;
  code = '';
 message ='';

 emailFormControl = new FormControl('', [
  Validators.required,
  Validators.email,
]);
nicknameFormControl = new FormControl('', [
  Validators.required,
  Validators.email,
]);
nameFormControl = new FormControl('', [
  Validators.required,
  Validators.email,
]);

passwordFormControl = new FormControl('', [
  Validators.required,
  Validators.email,
]);
password2FormControl = new FormControl('', [
  Validators.required,
  Validators.email,
]);
 signup(){
  // console.log("sign up clicked" +' '+ this.email+' '+this.password+' '+this.name)
  this.authService.signup(this.email,this.password)//כל זה מחזיר promise
  .then( value =>{
    this.authService.updateProfile(value.user,this.name);
    this.authService.addUser(value.user,this.name,this.nickname);
    console.log(value);
  }).then(value =>{
    this.router.navigate(['/'])//אחרי שעושים הרשמה זה עובר לדף הראשי טודוס
  }).catch(err =>{
    this.code=err.code;
    this.message=err.message;
    console.log(err);
  }) 
 }


  constructor(private authService:AuthService,private router:Router)
{

}

  ngOnInit() {
  }

}
