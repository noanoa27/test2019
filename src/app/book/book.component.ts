
import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { BooksService } from '../books.service';
@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked= new EventEmitter<any>();
  name;
  authName;
  key;

  showTheText=false;
  showTheButton=false;
 showEditField = false;
 text1;
 text2;
tempText1;
tempText2;

checkboxFlag: boolean;
checkChange()  {
  this.BooksService.updateDone(this.key,this.name,this.checkboxFlag);
 }

  show(){
    if(!this.showEditField)
    this.showTheText=true;
  } 
  hide(){
    this.showTheText=false;
  }
  save(){
    this.BooksService.update(this.key,this.text1,this.text2)
    this.showEditField=false;
  }
  cancel(){
    this.showEditField=false;
    this.name=this.tempText1;
    this.authName=this.tempText2;
  }
  showEdit() {
    this.showEditField = true;
    this.showTheText=false;
     this.tempText1 = this.name;
     this.tempText2 = this.authName;
  }
 
  constructor(private BooksService:BooksService) { }


  ngOnInit() {
    this.name = this.data.name;
   this.authName=this.data.authName;
   this.key=this.data.$key;
   this.checkboxFlag = this.data.done;

  }
 

}
